#language: pt

  Funcionalidade: Testes na plataforma alura

    @CT001
    Esquema do Cenário: Validar login na plataforma Alura
      Dado que acesso o site <siteAlura>
      Quando submeto email <email> e senha <senha>
      Entao devo ser direcionado a pagina inicial da paltaforma <dashboard>
      Exemplos:
        | siteAlura                   | email                               | senha       | dashboard   |
        | "https://www.alura.com.br/" | "pedro.figueiredo@betternow.com.br" | "Pedr.42@j" | "DASHBOARD" |




    @CT002
    Cenário: Validar opcao meus cursos
      Dado que acesso o site "https://www.alura.com.br/"
      Quando submeto email "pedro.figueiredo@betternow.com.br" e senha "Pedr.42@j"
      E dentro da pagina inicial clico em meu aprendizado, em seguida em meus cursos
      Entao serei direcionado a pagina de meus cursos da alura "MEUS CURSOS"



    @CT003
    Cenário: Validar campo de pesquisa
      Dado que acesso o site "https://www.alura.com.br/"
      Quando submeto email "pedro.figueiredo@betternow.com.br" e senha "Pedr.42@j"
      E na pagina inicial clico no icone de lupa e submeto "excel"
      Entao serei direcionado para o curso de excel do alura "Excel VBA"




    @CT004
    Cenário: Validar campo de pesquisa (procurando algo inexistente)
      Dado que acesso o site "https://www.alura.com.br/"
      Quando submeto email "pedro.figueiredo@betternow.com.br" e senha "Pedr.42@j"
      E na pagina inicial clico no icone de lupa, e submeto "ooooooo"
      Entao aparecera a seguinte mensagem de erro "Nenhum resultado encontrado para ooooooo. Tente outra busca!"



