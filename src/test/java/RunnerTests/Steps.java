package RunnerTests;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import WebDriver.ChromeDriverFactory;

import static org.junit.Assert.assertEquals;

public class Steps {

    private ChromeDriverFactory ChromeDriverFactory = new ChromeDriverFactory();
    private WebDriver driver;


        @Dado("que acesso o site {string}")
        public void acessarSite(String siteAlura) {
            driver = ChromeDriverFactory.configuracaoDriver();
            driver.get(siteAlura);

        }

        @Quando("submeto email {string} e senha {string}")
        public void submeteEmailESenha(String email, String senha) {
            driver.findElement(By.xpath("/html/body/div[1]/div/header/div/nav/div[1]/a[1]")).click();

            WebElement usuario = driver.findElement(By.id("login-email"));
            usuario.sendKeys(email);

            WebElement senhaAcesso = driver.findElement(By.id("password"));
            senhaAcesso.sendKeys(senha);
            driver.findElement(By.className("btn-login")).click();
        }

        @Entao("devo ser direcionado a pagina inicial da paltaforma {string}")
        public void validaPlataforma(String dashboard) {
            String validacao = driver.findElement(By.id("banner-title-link")).getText();
            assertEquals(dashboard, validacao);





        }
//        CT002

    @E("dentro da pagina inicial clico em meu aprendizado, em seguida em meus cursos")
    public void clicandoMeuAprendizadoEMeusCursos() throws InterruptedException {
            Thread.sleep(2000);
            driver.findElement(By.id("courseLabel")).click();
            driver.findElement(By.xpath("//*[@id=\"courseList\"]/a[2]")).click();
    }

    @Entao("serei direcionado a pagina de meus cursos da alura {string}")
    public void PaginaMeusCursos(String meusCursos) {
            String validacao = driver.findElement(By.className("banner-title")).getText();
            assertEquals(meusCursos, validacao);
    }



//    CT003

    @E("na pagina inicial clico no icone de lupa e submeto {string}")
    public void pesquisaExcel(String excel) throws InterruptedException {
            driver.findElement(By.className("headerBusca-icon")).click();
            Thread.sleep(3000);
            WebElement pesquisa = driver.findElement(By.className("skipKeyPress"));
            pesquisa.sendKeys(excel);
            driver.findElement(By.className("headerBusca-submit")).click();
            driver.findElement(By.className("busca-resultado-nome")).click();
    }

    @Entao("serei direcionado para o curso de excel do alura {string}")
    public void paginaExcel(String validacao) {
            String resultado = driver.findElement(By.className("formacao-headline-titulo")).getText();
            assertEquals(validacao, resultado);

    }


//    CT004


    @E("na pagina inicial clico no icone de lupa, e submeto {string}")
    public void pesquisaInvalida(String buscaInvalida) throws InterruptedException {
        driver.findElement(By.className("headerBusca-icon")).click();
        Thread.sleep(3000);
        WebElement pesquisa = driver.findElement(By.className("skipKeyPress"));
        pesquisa.sendKeys(buscaInvalida);
        driver.findElement(By.className("headerBusca-submit")).click();
    }


    @Entao("aparecera a seguinte mensagem de erro {string}")
    public void mensagemErro(String ooo) {
            String mensagemDeErro = driver.findElement(By.className("search-noResult")).getText();
//            String mensagemDeErro = driver.findElement(By.xpath("//*[@id=\"busca-resultados\"]/p")).getText();
            assertEquals(ooo,mensagemDeErro);
    }
}


