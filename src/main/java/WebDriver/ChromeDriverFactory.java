package WebDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class ChromeDriverFactory {

    private WebDriver driver;


    public WebDriver configuracaoDriver(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/SeleniumDriver/chromedriver.exe");
        this.driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this.driver;



    }
}
